package cn.da0ke.yhjava.core;

public class JRegex {
	
	/**
	 * 验证用户名
	 * @param password
	 * @return
	 */
	public static boolean isUsername(String username) {
		String rule = "^[a-zA-Z0-9_]{4,20}$";
		return username.matches(rule);
	}
	
	/**
	 * 验证密码 6-20位，大小写英文字母、数字及符号组成
	 * 
	 * @param password
	 * @return
	 */
	public static boolean isPassword(String password) {
		String rule = "^[^\u4e00-\u9fa5]{6,20}$";
		return password.matches(rule);
	}

	/**
	 * 验证手机号码格式
	 * 当号码段增加时，运营商并不会通知我，所以就干脆放宽了限制
	 */
	public static boolean isMobile(String mobile) {
		String rule = "^1[0-9]{10}$";
		return mobile!=null && mobile.matches(rule);
	}
	
	/**
	 * 验证固定电话格式
	 */
	public static boolean isTel(String tel) {
		String rule = "^[0-9-+]{6,}$";
		return tel.matches(rule);
	}

	/**
	 * 验证邮箱格式
	 */
	public static boolean isEmail(String email) {
		String rule = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
		return email.matches(rule);
	}
	
}
