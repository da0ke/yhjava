package cn.da0ke.yhjava.core;

import org.apache.commons.lang3.StringUtils;

import com.github.promeg.pinyinhelper.Pinyin;

public class JString {
	
	/**
	 * 
	 * @param mobile 必须是11位手机号码
	 * @return
	 */
	public static String formatMobile(final String mobile) {
		return mobile.substring(0, 3) + " " + mobile.substring(3, 7) + " " + mobile.substring(7);
	}

	public static boolean isEmpty(final CharSequence cs) {
		return StringUtils.isEmpty(cs);
	}
	
	public static boolean isNotEmpty(final CharSequence cs) {
		return StringUtils.isNotEmpty(cs);
	}
	
	public static boolean isAnyEmpty(CharSequence... css) {
		return StringUtils.isAnyEmpty(css);
	}
	
	public static boolean isBlank(final CharSequence cs) {
		return StringUtils.isBlank(cs);
	}
	
	public static boolean isNotBlank(final CharSequence cs) {
		return StringUtils.isNotBlank(cs);
	}
	
	public static boolean isAnyBlank(final CharSequence... css) {
		return StringUtils.isAnyBlank(css);
	}
	
	public static boolean containsWhiteSpace(final CharSequence s) {
		return StringUtils.containsWhitespace(s);
	}
	
	public static String deleteWhitespace(final String str) {
		return StringUtils.deleteWhitespace(str);
	}
	
	public static String trimToEmpty(final String str) {
		return StringUtils.trimToEmpty(str);
	}
	
	public static String trimToNull(final String str) {
		return StringUtils.trimToNull(str);
	}
	
	public static String[] splitByWhiteSpace(final String s) {
		if (JString.isBlank(s)) {
			return null;
		}
		
		return StringUtils.splitByWholeSeparator(s, null);
	}
	
	/**
	 * html文本，用于前台展示html内容
	 * 比如 u:text="${brief}"
	 * @param s
	 * @return
	 */
	public static String getHtmlText(String s) {
		if(!StringUtils.isEmpty(s)) {
			return s.replaceAll("<br/>", "<br>")
					.replaceAll("</br>", "<br>")
					// windows
					.replaceAll("\r\n", "<br>")
					// android
					.replaceAll("\n", "<br>")
					// mac
					.replaceAll("\r", "<br>")
					// 统一空格，否则容易出现段落开头空格不显示的问题
					.replaceAll(" ", "&nbsp;");
			
		} else {
			return s != null ? s : "";
		}
	}
	
	/**
	 * 纯文本，用于textarea中显示内容
	 * @param s
	 * @return
	 */
	public static String getPlainText(String s) {
		String text = getHtmlText(s);
		if (!StringUtils.isEmpty(text)) {
			text = text.replaceAll("<br>", "\r\n")
					.replaceAll("&nbsp;", " ");
		}
		return text;
	}
	
	/**
	 * 将输入字符串转为大写拼音
	 * 
	 * @param s
	 * @return
	 */
	public static String toPinyin(String s) {
		return Pinyin.toPinyin(s, "");
	}
	
}
