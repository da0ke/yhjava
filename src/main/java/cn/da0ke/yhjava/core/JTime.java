package cn.da0ke.yhjava.core;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class JTime {
	
	/** Date 转换为 LocalDateTime */
	public static LocalDateTime dateToLocalDateTime(Date date) {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}

	/** LocalDateTime转换为Date */
	public static Date localDateTimeToDate(LocalDateTime localDateTime) {
		ZoneId zoneId = ZoneId.systemDefault();
		ZonedDateTime zdt = localDateTime.atZone(zoneId);// Combines this date-time with a time-zone to create a
															// ZonedDateTime.
		return Date.from(zdt.toInstant());
	}
	
	/** LocalDateTime转换为时间戳，毫秒 */
	public static long localDateTimeToTimestamp(LocalDateTime localDateTime) {
		return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	}
	
	public static String format(LocalDateTime dateTime, String pattern) {
		return dateTime.format(DateTimeFormatter.ofPattern(pattern));
    }
	
	public static String format(LocalTime time, String pattern) {
		return time.format(DateTimeFormatter.ofPattern(pattern));
    }
	
	public static LocalDateTime strToLocalDateTime(String str, String pattern) {
		return LocalDateTime.parse(str, DateTimeFormatter.ofPattern(pattern));
	}
	
	public static LocalTime strToLocalTime(String str, String pattern) {
		return LocalTime.parse(str, DateTimeFormatter.ofPattern(pattern));
	}

}
