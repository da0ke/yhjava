package cn.da0ke.yhjava.core;

import java.math.BigDecimal;

public class JNumber {

	public static boolean isEmpty(Double n) {
		if (n == null) {
			return true;
		}

		return BigDecimal.valueOf(n).compareTo(BigDecimal.ZERO) == 0;
	}

	public static boolean isEmpty(Integer n) {
		if (n == null) {
			return true;
		}

		return BigDecimal.valueOf(n).compareTo(BigDecimal.ZERO) == 0;
	}

	public static boolean isNotEmpty(Double n) {
		return !isEmpty(n);
	}

	public static boolean isNotEmpty(Integer n) {
		return !isEmpty(n);
	}

	/**
	 * 是否为int
	 * 
	 * @param str 字符串
	 * @return boolean
	 */
	public static boolean isInt(String str) {
		if (str == null) {
			return false;
		}
		try {
			Integer.parseInt(str);
			return true;
		} catch (final NumberFormatException nfe) {
			return false;
		}
	}

	/**
	 * 字符串转换为整形，如果转换失败，返回0
	 * 
	 * @param s
	 * @return
	 */
	public static int strToInt(String s) {
		return strToInt(s, 0);
	}

	/**
	 * 字符串转换为整形，如果转换失败，返回默认值
	 * 
	 * @param s
	 * @param defValue
	 * @return
	 */
	public static int strToInt(String s, int defValue) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return defValue;
		}
	}

}
