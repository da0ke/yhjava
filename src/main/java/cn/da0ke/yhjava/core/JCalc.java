package cn.da0ke.yhjava.core;

import java.util.Calendar;

public class JCalc {
	
	/**
	 * 根据出生年份计算年龄-虚岁
	 */
	public static int getAgeByBirthYear(int birthYear) {
		return getFullAgeByBirthYear(birthYear) + 1;
	}
	
	/**
	 * 根据出生年份计算年龄-周岁
	 */
	public static int getFullAgeByBirthYear(int birthYear) {
		Calendar calendar = Calendar.getInstance();
		int thisYear = calendar.get(Calendar.YEAR);
		return (thisYear - birthYear);
	}

}
