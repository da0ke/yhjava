package cn.da0ke.yhjava.core;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Random;

public class JCaptcha {

	public static BufferedImage createImage(String code) {
		char display[] = { '0', ' ', '0', ' ', '0', ' ', '0' }, ran[] = { '0', '0', '0', '0' }, temp;
		Random rand = new Random();

		int width = 24 * code.length(), height = 30;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		Graphics g = image.getGraphics();
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.WHITE);

		g2d.fillRect(0, 0, width, height);

		for (int i = 0; i < 105; i++) {
			if (i % 2 == 0) {
				g2d.setColor(new Color(0xB9BDB8));
			} else if (i % 2 == 1) {
				g2d.setColor(new Color(0x51D6CA));
			}
			int x = rand.nextInt(width);
			int y = rand.nextInt(height);
			int xl = rand.nextInt(12);
			int yl = rand.nextInt(12);
			g2d.drawLine(x, y, x + xl, y + yl);
		}

		for (int i = 1; i < 3; i++) {
			int y = rand.nextInt(30);
			g2d.setColor(new Color(0xB9BDB8));
			g2d.drawLine(0, y, 95, y);
		}

		for (int i = 0; i < 4; i++) {
			if (i == 0) {
				g2d.translate(10, 0);
			} else {
				g2d.translate(20, 0);
			}

			int randNumber = rand.nextInt(code.length());
			temp = code.charAt(i);
			display[i * 2] = temp;
			ran[i] = temp;

			if (randNumber % 6 == 1) {
				g2d.setColor(new Color(0x0954DB));
			} else if (randNumber % 6 == 2) {
				g2d.setColor(Color.BLACK);
			} else if (randNumber % 6 == 3) {
				g2d.setColor(new Color(0x238500));
			} else if (randNumber % 6 == 4) {
				g2d.setColor(new Color(0xD63272));
			} else if (randNumber % 6 == 5) {
				g2d.setColor(new Color(0xB45d00));
			} else if (randNumber % 6 == 6) {
				g2d.setColor(Color.CYAN);
			} else {
				g2d.setColor(Color.GRAY);
			}

			int randSize = rand.nextInt(code.length());
			if (randSize % 6 == 1) {
				g2d.setFont(new Font("Sans", Font.PLAIN, 17));
			} else if (randSize % 6 == 2) {
				g2d.setFont(new Font("Sans", Font.PLAIN, 18));
			} else if (randSize % 6 == 3) {
				g2d.setFont(new Font("Sans", Font.PLAIN, 19));
			} else if (randSize % 6 == 4) {
				g2d.setFont(new Font("Sans", Font.PLAIN, 20));
			} else if (randSize % 6 == 5) {
				g2d.setFont(new Font("Sans", Font.PLAIN, 21));
			} else if (randSize % 6 == 6) {
				g2d.setFont(new Font("Sans", Font.PLAIN, 22));
			} else {
				g2d.setFont(new Font("Sans", Font.PLAIN, 23));
			}
			if (randSize % 2 == 1) {
				g2d.rotate((randSize % 6 * 3) * Math.PI / 180);
				g2d.drawString(String.valueOf(temp), 0, 25);
				g2d.rotate((-(randSize % 6 * 3)) * Math.PI / 180);
			} else {
				g2d.rotate((-(randSize % 6 * 3)) * Math.PI / 180);
				g2d.drawString(String.valueOf(temp), 0, 25);
				g2d.rotate((randSize % 6 * 3) * Math.PI / 180);
			}
		}

		g2d.dispose();

		return image;

	}

}
