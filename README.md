# YHJava

#### 介绍
Java开发封装类

#### 安装教程
```
repositories {
    maven { url "https://jitpack.io" }
}
dependencies {
    implementation 'com.gitee.da0ke:yhjava:Tag'
}
```

#### 使用说明

1.  JString
2.  JNumber
3.  JTime
4.  JCrypt
5.  JRegex
6.  JCalc
7.  JCaptcha


